/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../App';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  renderer.create(<App />);
});

// type Transport
//   @model
//   @auth(
//     rules: [
//       {allow: owner, ownerField: "owner", operations: [create, update, delete]},
//     ])
// {
//   id: ID!
//   number: String!
//   direction: String!
//   time: [Time] @connection(name: "TransportTimes")
//   owner: String
// }

// type Time
//   @model
//   @auth(
//     rules: [
//       {allow: owner, ownerField: "owner", operations: [create, update, delete]},
//     ])
// {
//   id: ID!
//   transport: Transport @connection(name: "TransportTimes")
//   time: String!
//   weekDay: String!
//   stop: String!
//   owner: String
// }

