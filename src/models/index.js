// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';



const { Transport, Time } = initSchema(schema);

export {
  Transport,
  Time
};