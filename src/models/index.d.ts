import { ModelInit, MutableModel, PersistentModelConstructor } from "@aws-amplify/datastore";





export declare class Transport {
  readonly id: string;
  readonly number: string;
  readonly direction: string;
  readonly time?: Time[];
  readonly owner?: string;
  constructor(init: ModelInit<Transport>);
  static copyOf(source: Transport, mutator: (draft: MutableModel<Transport>) => MutableModel<Transport> | void): Transport;
}

export declare class Time {
  readonly id: string;
  readonly transport?: Transport;
  readonly time: string;
  readonly weekDay: string;
  readonly stop: string;
  readonly owner?: string;
  constructor(init: ModelInit<Time>);
  static copyOf(source: Time, mutator: (draft: MutableModel<Time>) => MutableModel<Time> | void): Time;
}