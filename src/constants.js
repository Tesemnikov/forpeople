import { Dimensions } from 'react-native'

// colors
export const MAIN_COLOR = '#222222'
export const TITLE_COLOR = '#333333'
export const TITLE_FOR_BG_COLOR = '#454545'
export const MENU_LINK_COLOR = '#548eaa'
export const BG_COLOR = '#f3f3f3'
export const BG_BUTTON_COLOR = '#7aa1bd'
export const ICON_COLOR = '#548eaa'
export const WHITE = '#ffffff'
export const BORDER_COLOR = '#d4dddf'
export const SUCCESS_COLOR = '#4c7816'
export const WARNING_COLOR = '#fff7d7'
export const ERROR_COLOR = '#dd6c6b'

// size
export const TEXT_SIZE = 18
export const SUB_TEXT_SIZE = 15
export const TITLE_SIZE = 24
export const SUB_TITLE_SIZE = 20

export const MAIN_RADIUS = 4

export const win = Dimensions.get('window')
export const W_APP = win.width
export const H_APP = win.height

//font-family
export const MAIN_FONT_FAMILY = 'AvenirNext-DemiBold'

// style
export const mixinInformationContainer = {
  marginBottom: 10,
  backgroundColor: WHITE,
  padding: 10,
  borderBottomWidth: 1,
  borderColor: MENU_LINK_COLOR
}

export const mixinInformationTitle = {
  marginBottom: 5,
  fontSize: SUB_TITLE_SIZE,
  fontWeight: 'bold',
  color: MENU_LINK_COLOR
}

export const mixinInformationDescription = {
  fontSize: TEXT_SIZE,
  color: TITLE_FOR_BG_COLOR
}

export const goBack = navigation => () => navigation.goBack()

export const onScreen = (screen, navigation, obj) => () => {
  navigation.navigate(screen, obj)
}

export const goHome = navigation => () => navigation.popToTop()()