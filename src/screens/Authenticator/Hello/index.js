import React, { useEffect, useState } from 'react'
import {Text} from 'react-native'
import { Auth } from 'aws-amplify'
import * as Keychain from 'react-native-keychain'
import { Button } from '../../../components'
import { onScreen } from '../../../constants'

const Hello = ({ navigation }) => {
  const [loading, setLoading] = useState(false)
  useEffect(() => {
    setLoading(true)
    const key = async () => {
      try {
        const credentials = await Keychain.getInternetCredentials('auth')

        if (credentials) {
          const { username, password } = credentials
          const user = await Auth.signIn(username, password)
          setLoading(false)
          user && onScreen('USER', navigation)()
        } else {
          setLoading(false)
        }
      } catch (err) {
        console.log('error', err) // eslint-disable-line
        setLoading(false)
      }
    }
    key()
  }, []) // eslint-disable-line
  return (
    <>
      {!loading && (
        <>
          <Button title="Sign In" onPress={onScreen('SIGN_IN', navigation)} />
          <Text>или</Text>
          <Button title="Sign Up" onPress={onScreen('SIGN_UP', navigation)} />
        </>
      )}
    </>
  )
}

export { Hello }