import { SUB_TITLE_SIZE, MENU_LINK_COLOR, WHITE } from '../../constants'

export default {
  informationWrap: {
    backgroundColor: WHITE,
    paddingHorizontal: 10,
    title: {
      alignItems: 'center',
      marginBottom: 15,
      marginTop: 15,
      description: {
        color: MENU_LINK_COLOR,
        fontSize: SUB_TITLE_SIZE,
        fontWeight: 'bold'
      }
    }
  }
}