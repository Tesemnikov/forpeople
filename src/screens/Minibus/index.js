import React, { memo, useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next';
import { View, Text, ScrollView } from 'react-native'
import s from './style'
import { Header, Direction, Table, WeekBoard } from '../../components'
import {Transport} from '../../models'
import { DataStore } from '@aws-amplify/datastore';

const Minibus = memo(({ navigation }) => {

  const [schedule, setSchedule] = useState([])
  const getSchedule = async () => {
    const mess = await DataStore.query(Transport)
    console.log(22222, mess)
    setSchedule(mess)
  }
  // const getSchedule = async () => {
  //   try {
  //     const response = await DataStore.query(Time)
  //     console.log(response)
  //     setSchedule(response)
  //   } catch (e) {
  //     throw e
  //   }
  // }
  useEffect(() => {
    getSchedule()
    // const subscription = DataStore.observe(Time).subscribe(() => getSchedule())
    // return () => {
    //   subscription.unsubscribe()
    // }
  }, [])

  const { t } = useTranslation()
  // const dataSchedule = schedule
  const dataSchedule = schedule?.minibus?.fromMinsk?.friday

  return (
    <>
      <ScrollView>
        <Header onPress={navigation} title={`${t('header.titleMinibus')}`} />
        <View style={s.informationWrap}>
          <Direction />
          <WeekBoard />
          <View>
              <Table data={dataSchedule} title='Пятница' />
          </View>
          {/* <View style={s.informationWrap.title}>
            <Text style={s.informationWrap.title.description}>
              {`${t('bus.informationTitle')}`.toLocaleUpperCase()}
            </Text>
          </View> */}
          {/* <View style={s.informationWrap.container}>
            <NumberBus />
            <Schedule />
            <StreetsBus />
            <TechnicalInfo />
          </View> */}
        </View>
      </ScrollView>
    </>
  )
})

export { Minibus }