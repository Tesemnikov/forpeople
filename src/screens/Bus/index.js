import React, { memo } from 'react'
import { useTranslation } from 'react-i18next';
import { View, Text, ScrollView } from 'react-native'
import s from './style'
import { NumberBus, StreetsBus, TechnicalInfo, Schedule, Header } from '../../components'

const Bus = memo(({navigation}) => {

  const { t } = useTranslation()

  return (
    <>
      <ScrollView>
        <Header onPress={navigation} title={`${t('header.titleBus')}`}/>
        <View style={s.informationWrap}>
          <View style={s.informationWrap.title}>
            <Text style={s.informationWrap.title.description}>
              {`${t('bus.informationTitle')}`.toLocaleUpperCase()}
            </Text>
          </View>
          <View style={s.informationWrap.container}>
            <NumberBus />
            <Schedule />
            <StreetsBus />
            <TechnicalInfo />
          </View>
        </View>
      </ScrollView>
    </>
  )
})

export { Bus }