// @flow
// this is an auto generated file. This will be overwritten

export const createTransport = /* GraphQL */ `
  mutation CreateTransport(
    $input: CreateTransportInput!
    $condition: ModelTransportConditionInput
  ) {
    createTransport(input: $input, condition: $condition) {
      id
      number
      direction
      time {
        items {
          id
          time
          weekDay
          stop
          owner
        }
        nextToken
      }
      owner
    }
  }
`;
export const updateTransport = /* GraphQL */ `
  mutation UpdateTransport(
    $input: UpdateTransportInput!
    $condition: ModelTransportConditionInput
  ) {
    updateTransport(input: $input, condition: $condition) {
      id
      number
      direction
      time {
        items {
          id
          time
          weekDay
          stop
          owner
        }
        nextToken
      }
      owner
    }
  }
`;
export const deleteTransport = /* GraphQL */ `
  mutation DeleteTransport(
    $input: DeleteTransportInput!
    $condition: ModelTransportConditionInput
  ) {
    deleteTransport(input: $input, condition: $condition) {
      id
      number
      direction
      time {
        items {
          id
          time
          weekDay
          stop
          owner
        }
        nextToken
      }
      owner
    }
  }
`;
export const createTime = /* GraphQL */ `
  mutation CreateTime(
    $input: CreateTimeInput!
    $condition: ModelTimeConditionInput
  ) {
    createTime(input: $input, condition: $condition) {
      id
      transport {
        id
        number
        direction
        time {
          nextToken
        }
        owner
      }
      time
      weekDay
      stop
      owner
    }
  }
`;
export const updateTime = /* GraphQL */ `
  mutation UpdateTime(
    $input: UpdateTimeInput!
    $condition: ModelTimeConditionInput
  ) {
    updateTime(input: $input, condition: $condition) {
      id
      transport {
        id
        number
        direction
        time {
          nextToken
        }
        owner
      }
      time
      weekDay
      stop
      owner
    }
  }
`;
export const deleteTime = /* GraphQL */ `
  mutation DeleteTime(
    $input: DeleteTimeInput!
    $condition: ModelTimeConditionInput
  ) {
    deleteTime(input: $input, condition: $condition) {
      id
      transport {
        id
        number
        direction
        time {
          nextToken
        }
        owner
      }
      time
      weekDay
      stop
      owner
    }
  }
`;
