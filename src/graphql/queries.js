// @flow
// this is an auto generated file. This will be overwritten

export const getTransport = /* GraphQL */ `
  query GetTransport($id: ID!) {
    getTransport(id: $id) {
      id
      number
      direction
      time {
        items {
          id
          time
          weekDay
          stop
          owner
        }
        nextToken
      }
      owner
    }
  }
`;
export const listTransports = /* GraphQL */ `
  query ListTransports(
    $filter: ModelTransportFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTransports(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        number
        direction
        time {
          nextToken
        }
        owner
      }
      nextToken
    }
  }
`;
export const getTime = /* GraphQL */ `
  query GetTime($id: ID!) {
    getTime(id: $id) {
      id
      transport {
        id
        number
        direction
        time {
          nextToken
        }
        owner
      }
      time
      weekDay
      stop
      owner
    }
  }
`;
export const listTimes = /* GraphQL */ `
  query ListTimes(
    $filter: ModelTimeFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTimes(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        transport {
          id
          number
          direction
          owner
        }
        time
        weekDay
        stop
        owner
      }
      nextToken
    }
  }
`;
