// @flow
// this is an auto generated file. This will be overwritten

export const onCreateTransport = /* GraphQL */ `
  subscription OnCreateTransport($owner: String!) {
    onCreateTransport(owner: $owner) {
      id
      number
      direction
      time {
        items {
          id
          time
          weekDay
          stop
          owner
        }
        nextToken
      }
      owner
    }
  }
`;
export const onUpdateTransport = /* GraphQL */ `
  subscription OnUpdateTransport($owner: String!) {
    onUpdateTransport(owner: $owner) {
      id
      number
      direction
      time {
        items {
          id
          time
          weekDay
          stop
          owner
        }
        nextToken
      }
      owner
    }
  }
`;
export const onDeleteTransport = /* GraphQL */ `
  subscription OnDeleteTransport($owner: String!) {
    onDeleteTransport(owner: $owner) {
      id
      number
      direction
      time {
        items {
          id
          time
          weekDay
          stop
          owner
        }
        nextToken
      }
      owner
    }
  }
`;
export const onCreateTime = /* GraphQL */ `
  subscription OnCreateTime($owner: String!) {
    onCreateTime(owner: $owner) {
      id
      transport {
        id
        number
        direction
        time {
          nextToken
        }
        owner
      }
      time
      weekDay
      stop
      owner
    }
  }
`;
export const onUpdateTime = /* GraphQL */ `
  subscription OnUpdateTime($owner: String!) {
    onUpdateTime(owner: $owner) {
      id
      transport {
        id
        number
        direction
        time {
          nextToken
        }
        owner
      }
      time
      weekDay
      stop
      owner
    }
  }
`;
export const onDeleteTime = /* GraphQL */ `
  subscription OnDeleteTime($owner: String!) {
    onDeleteTime(owner: $owner) {
      id
      transport {
        id
        number
        direction
        time {
          nextToken
        }
        owner
      }
      time
      weekDay
      stop
      owner
    }
  }
`;
