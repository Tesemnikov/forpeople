// @flow
import React, { memo, useState, useEffect } from 'react'
import { Platform, StyleSheet, TouchableWithoutFeedback, View, Text } from 'react-native'
import type { TextStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import { W_APP, MENU_LINK_COLOR, WHITE } from '../../constants'

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    alignSelf: 'center'
  },
  h: {
    width: W_APP - 60,
    paddingTop: Platform.OS === 'ios' ? 15 : 5,
    paddingBottom: 10,
    textAlign: 'center',
    textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 1,
    ...ifIphoneX(
      {
        fontSize: Platform.OS === 'ios' ? 20 : 20
      },
      {
        fontSize: Platform.OS === 'ios' ? 20 : 30
      }
    )
  }
})

type ButtonT = {
  title: string,
  cancel: boolean,
  onPress: Function,
  textStyle: TextStyleProp
}

const Button = memo<ButtonT>(({ title, onPress, textStyle, cancel }) => {
  const { container, h } = styles
  const [bg, setBg] = useState(WHITE)

  useEffect(() => {
    setBg(cancel ? 'gray' : 'green')
  }, []) // eslint-disable-line

  return (
    <View style={[container, { borderColor: MENU_LINK_COLOR }]}>
        <TouchableWithoutFeedback
          onPress={onPress}
          onPressIn={() => setBg(cancel ? WHITE : MENU_LINK_COLOR)}
          onPressOut={() => setBg(cancel ? MENU_LINK_COLOR : WHITE)}
        >
          <Text style={[h, textStyle, { color: bg, textShadowColor: 'gray' }]}>{title}</Text>
        </TouchableWithoutFeedback>
    </View>
  )
})

export { Button }