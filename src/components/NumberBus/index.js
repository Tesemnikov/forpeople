import React from 'react'
import { View, Text } from 'react-native'
import { useTranslation } from 'react-i18next';
import s from './style'

const NumberBus = () => {

  const { t } = useTranslation()

  return (
    <View style={s.container}>
      <Text style={s.container.title}>{t('bus.numberBusTitle')}</Text>
      <View>
        <View>
          <Text style={s.container.description}>
            {t('bus.numberBus')}
          </Text>
        </View>
      </View>
    </View>
  )
}

export { NumberBus }