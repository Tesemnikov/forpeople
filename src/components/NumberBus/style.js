import { mixinInformationTitle, mixinInformationContainer, mixinInformationDescription } from '../../constants'

export default {
  container: {
    ...mixinInformationContainer,
    title: {
      ...mixinInformationTitle
    },
    description: {
      ...mixinInformationDescription
    }
  }
}