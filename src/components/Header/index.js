import React, { memo } from 'react'
import { useNavigation } from '@react-navigation/native';
import { TouchableOpacity, View, Text } from 'react-native'
import Fontisto from 'react-native-vector-icons/Fontisto'
import s from './style'
import {ICON_COLOR} from '../../constants'

Fontisto.loadFont();


const Header = memo(({ title, iconLeft = 'nav-icon-a', iconRight = 'player-settings', onPress}) => {
  
  return (
    <View style={s.container}>
      <View style={s.iconWrap}>
        <TouchableOpacity onPress={()=> onPress.openDrawer()}>
          <Fontisto name={iconLeft} size={20} color={ICON_COLOR} />
        </TouchableOpacity>
      </View>
      <View>
        <Text style={s.title}>{`${title}`.toUpperCase()}</Text>
      </View>
      <View>
        <TouchableOpacity>
          <Fontisto name={iconRight} size={25} color={ICON_COLOR} />
        </TouchableOpacity>
      </View>
    </View>
  )
})

export { Header }