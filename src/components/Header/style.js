import { TITLE_SIZE, MENU_LINK_COLOR } from '../../constants'

export default {
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: '#fff',
    paddingHorizontal: 10
  },
  title: {
    fontSize: TITLE_SIZE,
    color: MENU_LINK_COLOR,
    fontWeight: 'bold'
  }
}