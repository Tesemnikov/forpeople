import React, {useEffect, useState, memo} from 'react'
import { View, Text } from 'react-native'
import { useTranslation } from 'react-i18next'
import s from './style'

const url = 'https://gitlab.com/Tesemnikov/database/-/raw/master/forPeople/dataBase.json'

const StreetsBus = memo(() => {

  const { t } = useTranslation()
  const [streets, setStreets] = useState([])
  const getStreets = async() => {
    try {
      const response = await fetch(url)
      const dataBase = await response.json()
      setStreets(dataBase)
    } catch (e) {
      throw e
    }
  }
  const locale = 'en'//do it logic language
  useEffect(() => {
    getStreets()
  }, [])

  return (
    <View style={s.container}>
      <Text style={s.container.title}>{t('bus.streetsTitle')}</Text>
      <View>
        <View>
          {streets[0]?.bus.streets.map((element) => (
            locale === 'en' ? //do it logic language 
            <View key={element.id}>
              <Text style={s.container.description}>- {element.street[0]}</Text>
            </View>
            :
            <View key={element.id}>
              <Text style={s.container.description}>- {element.street[1]}</Text>
            </View>
          ))}
        </View>
      </View>
    </View>
  )
})

export { StreetsBus }