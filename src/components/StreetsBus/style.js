import { mixinInformationDescription, mixinInformationTitle, mixinInformationContainer } from '../../constants'

export default {
  container: {
    ...mixinInformationContainer,
    title: {
      ...mixinInformationTitle
    },
    description: {
      ...mixinInformationDescription
    }
  }
}