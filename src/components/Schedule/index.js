import React, { useState, useEffect } from 'react'
import { View, Text } from 'react-native'
import s from './style'
import { useTranslation } from 'react-i18next'

const url = 'https://gitlab.com/Tesemnikov/database/-/raw/master/forPeople/dataBase.json'

const Schedule = () => {

  const { t } = useTranslation()
  const [dataSchedule, setDataSchedule] = useState([])
  const getDatabase = async () => {
    try {
      const response = await fetch(url)
      const data = await response.json()
      setDataSchedule(data)
    } catch (e) {
      throw e
    }
  }
  useEffect(() => {
    getDatabase()
  }, [])

  return (
    <View style={s.container}>
      <Text style={s.container.title}>{t('bus.schedule.title')}</Text>
      <View>
        <View>
          <View>
            <View style={s.tehnicalBlock}>
              <Text style={s.container.description}>
                {t('bus.schedule.fromMinsk')}
              </Text>
              <View style={s.container.subDescription}>
                <Text style={s.container.subDescription.title}>
                  {t('bus.schedule.weekdays')}
                </Text>
                <Text style={s.container.description}>
                  {dataSchedule[0]?.bus.schedule.fromMinsk.weekdays.map((elem) => (
                    elem === '17:01' ? elem : `${elem}  `
                  ))}
                      </Text>
              </View>
              <View style={s.container.subDescription}>
                <Text style={s.container.subDescription.title}>
                  {t('bus.schedule.weekends')}
                </Text>
                <Text style={s.container.description}>
                {dataSchedule[0]?.bus.schedule.fromMinsk.weekends.map((elem) => (
                    elem === '19:12' ? elem : `${elem}  `
                  ))}
                      </Text>
              </View>
            </View>

            <View>
              <Text style={s.container.description}>
                {t('bus.schedule.toMinsk')}
              </Text>
              <View style={s.container.subDescription}>
                <Text style={s.container.subDescription.title}>
                  {t('bus.schedule.weekdays')}
                </Text>
                <Text style={s.container.description}>
                {dataSchedule[0]?.bus.schedule.toMinsk.weekdays.map((elem) => (
                    elem === '17:52' ? elem : `${elem}  `
                  ))}
                      </Text>
              </View>
              <View style={s.container.subDescription}>
                <Text style={s.container.subDescription.title}>
                  {t('bus.schedule.weekends')}
                </Text>
                <Text style={s.container.description}>
                {dataSchedule[0]?.bus.schedule.toMinsk.weekends.map((elem) => (
                    elem === '20:00' ? elem : `${elem}  `
                  ))}
                      </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  )
}

export { Schedule }