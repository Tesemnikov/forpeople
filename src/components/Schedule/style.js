import { SUB_TEXT_SIZE, mixinInformationTitle, mixinInformationContainer, mixinInformationDescription, WHITE, MENU_LINK_COLOR } from '../../constants'

export default {
  container: {
    ...mixinInformationContainer,
    title: {
      ...mixinInformationTitle
    },
    description: {
      ...mixinInformationDescription
    },
    subDescription: {
      paddingTop: 5,
      title: {
        fontSize: SUB_TEXT_SIZE,
        fontWeight: 'bold',
        color: MENU_LINK_COLOR
      }
    }
  },
  tehnicalBlock: {
    borderBottomWidth: 1,
    borderColor: WHITE,
    marginBottom: 5,
    paddingBottom: 5
  }
}