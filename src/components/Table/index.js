import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import s from './style'

const Table = ({
  title,
  data
}) => {

  return (
    <ScrollView style={s.container}>
      <View style={s.titleWrap}>
        <View style={s.titleWrap.empty} />
        <View style={s.titleWrap.description} >
          <Text style={s.titleWrap.description.title}>{title}</Text>
        </View>
      </View>
      {data?.hours?.map((element, id) => (
        <View key={id} style={s.schedule}>
          <View style={s.schedule.col} >
            <Text style={s.schedule.col.title}>{element}</Text>
          </View>
          <View style={s.schedule.row} >
              <View key={id}>
                <Text style={s.schedule.row.title}>{data?.minutes[id]}</Text>
              </View>
          </View>
        </View>
      ))
      }
    </ScrollView>
  )
}

export { Table }