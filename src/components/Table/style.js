export default {
  container: {
    borderWidth: 1,
    borderColor: '#ddd',
    marginBottom: 10
  },
  titleWrap: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'row',
    height: 35,
    borderBottomWidth: 1,
    borderColor: '#ddd',
    backgroundColor: '#eee',
    empty: {
      flex: 0.1,
      alignItems: 'center',
      justifyContent: 'center',
      borderRightWidth: 1,
      borderColor: '#ddd',
      backgroundColor: '#eee'
    },
    description: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      paddingHorizontal: 10,
      title: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#454545'
      }
    }
  },
  schedule: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'row',
    height: 35,
    borderBottomWidth: 1,
    borderColor: '#ddd',
    col: {
      flex: 0.1,
      backgroundColor: '#eee',
      alignItems: 'center',
      justifyContent: 'center',
      borderRightWidth: 1,
      borderColor: '#ddd',
      title: {
        fontWeight: 'bold',
        fontSize: 18
      }
    },
    row: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'flex-start',
      flexDirection: 'row',
      paddingHorizontal: 10,
      title: {
        fontSize: 18,
        marginRight: 5
      }
    }
  }
}