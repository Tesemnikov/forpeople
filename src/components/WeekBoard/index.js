import React from 'react'
import { View, Text } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import s from './style'

const WeekBoard = () => {

  const arr = [
    {
      id: 0,
      name: "ПН"
    },
    {
      id: 1,
      name: "ВТ"
    },
    {
      id: 2,
      name: "СР"
    },
    {
      id: 3,
      name: "ЧТ"
    },
    {
      id: 4,
      name: "ПТ"
    },
    {
      id: 5,
      name: "СБ"
    },
    {
      id: 6,
      name: "ВС"
    },
  ]

  return (
    <View style={s.container}>
      <View style={s.wrapper}>
        {arr.map((el) => (
          <TouchableOpacity style={s.wrapper.item} key={el.id} onPress={() => {
            return console.log(el.name === 'ПН' ? 123 : 456)
          }}>
            <View style={s.wrapper.item.block}>
              <Text style={s.wrapper.item.text}>{el.name}</Text>
            </View>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  )
}

export { WeekBoard }