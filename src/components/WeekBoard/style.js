import {MENU_LINK_COLOR, WHITE} from '../../constants'

export default {
  container: {
    paddingVertical: 10,
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'center', 
    borderRadius: 10, 
    borderWidth: 2,
    borderColor: MENU_LINK_COLOR,
    item: {
      padding: 7,
      block: {
        padding: 9,
        backgroundColor: MENU_LINK_COLOR,
        borderRadius: 10 
      },
      text: {
        color: WHITE,
        fontSize: 16
      }
    }
  }
}