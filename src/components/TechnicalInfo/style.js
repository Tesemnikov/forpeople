import { mixinInformationTitle, mixinInformationDescription, mixinInformationContainer, WHITE} from '../../constants'

export default {
  container: {
    ...mixinInformationContainer,
    title: {
      ...mixinInformationTitle
    },
    description: {
      ...mixinInformationDescription
    }
  },
  tehnicalBlock: {
    borderBottomWidth: 1,
    borderColor: WHITE,
    marginBottom: 5,
    paddingBottom: 5
  }
}