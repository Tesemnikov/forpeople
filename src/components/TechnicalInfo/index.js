import React from 'react'
import { View, Text } from 'react-native'
import { useTranslation } from 'react-i18next';
import s from './style'

const TechnicalInfo = () => {

  const {t} = useTranslation()


  return (
    <View style={s.container}>
      <Text style={s.container.title}>Техническая информация:</Text>
      <View>
        <View>
          <View>
            <View style={s.tehnicalBlock}>
              <Text style={s.container.description}>
                {t('bus.technicalInfo.distanceMinskMachulishi')}
              </Text>
              <Text style={s.container.description}>Остановок: 19</Text>
            </View>
            <View>
              <Text style={s.container.description}>
              {t('bus.technicalInfo.distanceMachulishiMinsk')}
              </Text>
              <Text style={s.container.description}>Остановок: 18</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  )
}

export { TechnicalInfo }