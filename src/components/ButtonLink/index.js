// @flow
import React, { memo } from 'react'
import { StyleSheet, Platform, TouchableOpacity, Text } from 'react-native'
import type { TextStyleProp, ViewStyleProp } from 'react-native/Libraries/StyleSheet/StyleSheet'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  h: {
    fontSize: 15,
    textDecorationLine: 'underline'
  }
})

type ButtonLinkT = {
  title: string,
  textStyle: TextStyleProp,
  viewStyle: ViewStyleProp,
  onPress: Function
}

const ButtonLink = memo<ButtonLinkT>(({ title, textStyle, viewStyle, onPress }) => {
  const { container, h } = styles
  const fontSize = 18
  const size = Platform.OS === 'ios' ? 13 : 13
  return (
    <TouchableOpacity onPress={onPress} style={[container, viewStyle]}>
      <Text
        style={[
          h,
          textStyle,
          { fontSize: fontSize || size, color: true ? 'blue' : 'gray', textShadowColor: 'gray' }
        ]}
      >
        {title}
      </Text>
    </TouchableOpacity>
  )
})

export { ButtonLink }