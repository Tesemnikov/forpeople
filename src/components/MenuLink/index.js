import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Fontisto'
import s from './style'
import { WHITE } from '../../constants'

Icon.loadFont();

const MenuLink = ({
  name,
  icon,
  onPress,
  link
}) => {

  const locale = 'en'

  return (
    <View style={s.link}>
      <View style={s.icon}>
        <Icon size={25} name={icon} color={WHITE} />
      </View>
      <TouchableOpacity style={s.wrapTitle} onPress={() => onPress.navigate(link)}>
        <Text style={s.title}>{locale !== 'en' ? name[0] : name[1]}</Text>
      </TouchableOpacity>
    </View>
  )
}
export { MenuLink }
