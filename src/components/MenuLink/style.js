import { WHITE, MAIN_FONT_FAMILY } from '../../constants'

export default {
  link: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 20,
    marginRight: 15,
    marginLeft: 15,
    borderBottomWidth: 1,
    borderColor: WHITE,
  },
  icon: {
    marginRight: 15,
    width: 35,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 5
  },
  wrapTitle: {
    paddingBottom: 5,
  },
  title: {
    fontSize: 20,
    color: WHITE,
    fontFamily: MAIN_FONT_FAMILY,
    fontWeight: 'bold',
    textTransform: 'uppercase'
  }
}