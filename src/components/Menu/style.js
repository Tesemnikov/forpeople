import { MAIN_FONT_FAMILY, MENU_LINK_COLOR, WHITE, MAIN_BACKGROUND } from '../../constants'
export default {
  container: {
    flex: 1,
    backgroundColor: MENU_LINK_COLOR
  },
  wrapImage: {
    marginTop: 50,
    alignItems: 'center',
    marginBottom: 30
  },
  image: {
    width: 70,
    height: 70
  },
  wrapTitle: {
    alignItems: 'center',
    marginBottom: 50
  },
  title: {
    fontSize: 27,
    color: WHITE,
    fontFamily: MAIN_FONT_FAMILY
  }
}