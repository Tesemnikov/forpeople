import React from 'react'
import { View, Image } from 'react-native'
import { MenuLink } from '../index'
import s from './style'

const Menu = ({navigation}) => {

  const arrayLinks = [
    {
      iconName: 'home',
      title: ['Home', 'Главная'],
      linkName: 'Home'
    },
    {
      iconName: 'truck',
      title: ['Bus', 'Автобус'],
      linkName: 'Bus'
    },
    {
      iconName: 'bus',
      title: ['Minibus', 'Маршрутка'],
      linkName: 'Minibus'
    },
    {
      iconName: 'person',
      title: ['Auth', 'Авторизация'],
      linkName: 'StackNavigator'
    }
  ]

  return (
    <View style={s.container}>
      <View style={s.wrapImage}>
        <Image style={s.image} source={require('./images/information.png')} />
      </View>
      <View>
        {arrayLinks.map((el, id) => {
          return <MenuLink icon={el.iconName} link={el.linkName} name={el.title} key={id} onPress={navigation} />
        })}
      </View>
    </View>
  )
}

export { Menu }