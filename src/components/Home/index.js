import React, { memo } from 'react'
import { View, Text, ScrollView } from 'react-native'
import s from './style'
import { Header } from '../Header'
import { useTranslation } from 'react-i18next'

const Home = memo(({ navigation }) => {

  const { t } = useTranslation()

  return (
    <ScrollView>
      <Header title={`${t('header.titleHome')}`} onPress={navigation} />
      <View>
        <View><Text>Weather</Text></View>
        <View><Text>Currency</Text></View>
        <View><Text>News</Text></View>
      </View>
    </ScrollView>
  )
})

export { Home }
