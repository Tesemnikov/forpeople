import { TITLE_SIZE, MENU_LINK_COLOR } from '../../constants'

export default {
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 10
  },
  title: {
    fontSize: TITLE_SIZE,
    fontWeight: 'bold',
    color: MENU_LINK_COLOR
  }
}