import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Fontisto'
import { MENU_LINK_COLOR } from '../../constants'
import { useTranslation } from 'react-i18next'
import s from './style'

Icon.loadFont();

const Direction = () => {

  const { t } = useTranslation()

  return (
    <View style={s.container}>
      <View>
        <Text style={s.title}>{t(`minibus.direction.toMinsk`)}</Text>
      </View>
      <TouchableOpacity onPress={() => console.log('DIRECTION')}>
        <Icon name='arrow-swap' size={30} color={MENU_LINK_COLOR} />
      </TouchableOpacity>
    </View>
  )
}

export { Direction }