import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Home, Menu } from '../../components';
import { Bus, Minibus } from '../../screens'
import {StackNavigator} from '../'

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Minibus" drawerContent={(props) => <Menu {...props} />}>
        <Drawer.Screen name="Home" component={Home} />
        <Drawer.Screen name="Bus" component={Bus} />
        <Drawer.Screen name="Minibus" component={Minibus} />
        <Drawer.Screen name="StackNavigator" component={StackNavigator} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

export { DrawerNavigator }