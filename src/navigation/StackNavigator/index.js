import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Hello, SignUp, ConfirmSignUp, SignIn, User, Forgot, ForgotPassSubmit } from '../../screens/Authenticator'
const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="Hello"
    >
      <Stack.Screen name="Hello" component={Hello} />
      <Stack.Screen name="SIGN_UP" component={SignUp} />
      <Stack.Screen name="CONFIRM_SIGN_UP" component={ConfirmSignUp} />
      <Stack.Screen name="SIGN_IN" component={SignIn} />
      <Stack.Screen name="FORGOT" component={Forgot} />
      <Stack.Screen name="FORGOT_PASSWORD_SUBMIT" component={ForgotPassSubmit} />
      <Stack.Screen name="USER" component={User} />
    </Stack.Navigator>
  );
}

export { StackNavigator }