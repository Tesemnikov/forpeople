/* @flow */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateTransportInput = {|
  id?: ?string,
  number: string,
  direction: string,
  owner?: ?string,
|};

export type ModelTransportConditionInput = {|
  number?: ?ModelStringInput,
  direction?: ?ModelStringInput,
  and?: ?Array< ?ModelTransportConditionInput >,
  or?: ?Array< ?ModelTransportConditionInput >,
  not?: ?ModelTransportConditionInput,
|};

export type ModelStringInput = {|
  ne?: ?string,
  eq?: ?string,
  le?: ?string,
  lt?: ?string,
  ge?: ?string,
  gt?: ?string,
  contains?: ?string,
  notContains?: ?string,
  between?: ?Array< ?string >,
  beginsWith?: ?string,
  attributeExists?: ?boolean,
  attributeType?: ?ModelAttributeTypes,
  size?: ?ModelSizeInput,
|};

export type ModelAttributeTypes =
  "binary" |
  "binarySet" |
  "bool" |
  "list" |
  "map" |
  "number" |
  "numberSet" |
  "string" |
  "stringSet" |
  "_null";


export type ModelSizeInput = {|
  ne?: ?number,
  eq?: ?number,
  le?: ?number,
  lt?: ?number,
  ge?: ?number,
  gt?: ?number,
  between?: ?Array< ?number >,
|};

export type UpdateTransportInput = {|
  id: string,
  number?: ?string,
  direction?: ?string,
  owner?: ?string,
|};

export type DeleteTransportInput = {|
  id?: ?string,
|};

export type CreateTimeInput = {|
  id?: ?string,
  time: string,
  weekDay: string,
  stop: string,
  owner?: ?string,
  timeTransportId?: ?string,
|};

export type ModelTimeConditionInput = {|
  time?: ?ModelStringInput,
  weekDay?: ?ModelStringInput,
  stop?: ?ModelStringInput,
  and?: ?Array< ?ModelTimeConditionInput >,
  or?: ?Array< ?ModelTimeConditionInput >,
  not?: ?ModelTimeConditionInput,
|};

export type UpdateTimeInput = {|
  id: string,
  time?: ?string,
  weekDay?: ?string,
  stop?: ?string,
  owner?: ?string,
  timeTransportId?: ?string,
|};

export type DeleteTimeInput = {|
  id?: ?string,
|};

export type ModelTransportFilterInput = {|
  id?: ?ModelIDInput,
  number?: ?ModelStringInput,
  direction?: ?ModelStringInput,
  owner?: ?ModelStringInput,
  and?: ?Array< ?ModelTransportFilterInput >,
  or?: ?Array< ?ModelTransportFilterInput >,
  not?: ?ModelTransportFilterInput,
|};

export type ModelIDInput = {|
  ne?: ?string,
  eq?: ?string,
  le?: ?string,
  lt?: ?string,
  ge?: ?string,
  gt?: ?string,
  contains?: ?string,
  notContains?: ?string,
  between?: ?Array< ?string >,
  beginsWith?: ?string,
  attributeExists?: ?boolean,
  attributeType?: ?ModelAttributeTypes,
  size?: ?ModelSizeInput,
|};

export type ModelTimeFilterInput = {|
  id?: ?ModelIDInput,
  time?: ?ModelStringInput,
  weekDay?: ?ModelStringInput,
  stop?: ?ModelStringInput,
  owner?: ?ModelStringInput,
  and?: ?Array< ?ModelTimeFilterInput >,
  or?: ?Array< ?ModelTimeFilterInput >,
  not?: ?ModelTimeFilterInput,
|};

export type CreateTransportMutationVariables = {|
  input: CreateTransportInput,
  condition?: ?ModelTransportConditionInput,
|};

export type CreateTransportMutation = {|
  createTransport: ? {|
    __typename: "Transport",
    id: string,
    number: string,
    direction: string,
    time: ? {|
      __typename: "ModelTimeConnection",
      items: ? Array<? {|
        __typename: "Time",
        id: string,
        time: string,
        weekDay: string,
        stop: string,
        owner: ?string,
      |} >,
      nextToken: ?string,
    |},
    owner: ?string,
  |},
|};

export type UpdateTransportMutationVariables = {|
  input: UpdateTransportInput,
  condition?: ?ModelTransportConditionInput,
|};

export type UpdateTransportMutation = {|
  updateTransport: ? {|
    __typename: "Transport",
    id: string,
    number: string,
    direction: string,
    time: ? {|
      __typename: "ModelTimeConnection",
      items: ? Array<? {|
        __typename: "Time",
        id: string,
        time: string,
        weekDay: string,
        stop: string,
        owner: ?string,
      |} >,
      nextToken: ?string,
    |},
    owner: ?string,
  |},
|};

export type DeleteTransportMutationVariables = {|
  input: DeleteTransportInput,
  condition?: ?ModelTransportConditionInput,
|};

export type DeleteTransportMutation = {|
  deleteTransport: ? {|
    __typename: "Transport",
    id: string,
    number: string,
    direction: string,
    time: ? {|
      __typename: "ModelTimeConnection",
      items: ? Array<? {|
        __typename: "Time",
        id: string,
        time: string,
        weekDay: string,
        stop: string,
        owner: ?string,
      |} >,
      nextToken: ?string,
    |},
    owner: ?string,
  |},
|};

export type CreateTimeMutationVariables = {|
  input: CreateTimeInput,
  condition?: ?ModelTimeConditionInput,
|};

export type CreateTimeMutation = {|
  createTime: ? {|
    __typename: "Time",
    id: string,
    transport: ? {|
      __typename: "Transport",
      id: string,
      number: string,
      direction: string,
      time: ? {|
        __typename: "ModelTimeConnection",
        nextToken: ?string,
      |},
      owner: ?string,
    |},
    time: string,
    weekDay: string,
    stop: string,
    owner: ?string,
  |},
|};

export type UpdateTimeMutationVariables = {|
  input: UpdateTimeInput,
  condition?: ?ModelTimeConditionInput,
|};

export type UpdateTimeMutation = {|
  updateTime: ? {|
    __typename: "Time",
    id: string,
    transport: ? {|
      __typename: "Transport",
      id: string,
      number: string,
      direction: string,
      time: ? {|
        __typename: "ModelTimeConnection",
        nextToken: ?string,
      |},
      owner: ?string,
    |},
    time: string,
    weekDay: string,
    stop: string,
    owner: ?string,
  |},
|};

export type DeleteTimeMutationVariables = {|
  input: DeleteTimeInput,
  condition?: ?ModelTimeConditionInput,
|};

export type DeleteTimeMutation = {|
  deleteTime: ? {|
    __typename: "Time",
    id: string,
    transport: ? {|
      __typename: "Transport",
      id: string,
      number: string,
      direction: string,
      time: ? {|
        __typename: "ModelTimeConnection",
        nextToken: ?string,
      |},
      owner: ?string,
    |},
    time: string,
    weekDay: string,
    stop: string,
    owner: ?string,
  |},
|};

export type GetTransportQueryVariables = {|
  id: string,
|};

export type GetTransportQuery = {|
  getTransport: ? {|
    __typename: "Transport",
    id: string,
    number: string,
    direction: string,
    time: ? {|
      __typename: "ModelTimeConnection",
      items: ? Array<? {|
        __typename: "Time",
        id: string,
        time: string,
        weekDay: string,
        stop: string,
        owner: ?string,
      |} >,
      nextToken: ?string,
    |},
    owner: ?string,
  |},
|};

export type ListTransportsQueryVariables = {|
  filter?: ?ModelTransportFilterInput,
  limit?: ?number,
  nextToken?: ?string,
|};

export type ListTransportsQuery = {|
  listTransports: ? {|
    __typename: "ModelTransportConnection",
    items: ? Array<? {|
      __typename: "Transport",
      id: string,
      number: string,
      direction: string,
      time: ? {|
        __typename: "ModelTimeConnection",
        nextToken: ?string,
      |},
      owner: ?string,
    |} >,
    nextToken: ?string,
  |},
|};

export type GetTimeQueryVariables = {|
  id: string,
|};

export type GetTimeQuery = {|
  getTime: ? {|
    __typename: "Time",
    id: string,
    transport: ? {|
      __typename: "Transport",
      id: string,
      number: string,
      direction: string,
      time: ? {|
        __typename: "ModelTimeConnection",
        nextToken: ?string,
      |},
      owner: ?string,
    |},
    time: string,
    weekDay: string,
    stop: string,
    owner: ?string,
  |},
|};

export type ListTimesQueryVariables = {|
  filter?: ?ModelTimeFilterInput,
  limit?: ?number,
  nextToken?: ?string,
|};

export type ListTimesQuery = {|
  listTimes: ? {|
    __typename: "ModelTimeConnection",
    items: ? Array<? {|
      __typename: "Time",
      id: string,
      transport: ? {|
        __typename: "Transport",
        id: string,
        number: string,
        direction: string,
        owner: ?string,
      |},
      time: string,
      weekDay: string,
      stop: string,
      owner: ?string,
    |} >,
    nextToken: ?string,
  |},
|};

export type OnCreateTransportSubscriptionVariables = {|
  owner: string,
|};

export type OnCreateTransportSubscription = {|
  onCreateTransport: ? {|
    __typename: "Transport",
    id: string,
    number: string,
    direction: string,
    time: ? {|
      __typename: "ModelTimeConnection",
      items: ? Array<? {|
        __typename: "Time",
        id: string,
        time: string,
        weekDay: string,
        stop: string,
        owner: ?string,
      |} >,
      nextToken: ?string,
    |},
    owner: ?string,
  |},
|};

export type OnUpdateTransportSubscriptionVariables = {|
  owner: string,
|};

export type OnUpdateTransportSubscription = {|
  onUpdateTransport: ? {|
    __typename: "Transport",
    id: string,
    number: string,
    direction: string,
    time: ? {|
      __typename: "ModelTimeConnection",
      items: ? Array<? {|
        __typename: "Time",
        id: string,
        time: string,
        weekDay: string,
        stop: string,
        owner: ?string,
      |} >,
      nextToken: ?string,
    |},
    owner: ?string,
  |},
|};

export type OnDeleteTransportSubscriptionVariables = {|
  owner: string,
|};

export type OnDeleteTransportSubscription = {|
  onDeleteTransport: ? {|
    __typename: "Transport",
    id: string,
    number: string,
    direction: string,
    time: ? {|
      __typename: "ModelTimeConnection",
      items: ? Array<? {|
        __typename: "Time",
        id: string,
        time: string,
        weekDay: string,
        stop: string,
        owner: ?string,
      |} >,
      nextToken: ?string,
    |},
    owner: ?string,
  |},
|};

export type OnCreateTimeSubscriptionVariables = {|
  owner: string,
|};

export type OnCreateTimeSubscription = {|
  onCreateTime: ? {|
    __typename: "Time",
    id: string,
    transport: ? {|
      __typename: "Transport",
      id: string,
      number: string,
      direction: string,
      time: ? {|
        __typename: "ModelTimeConnection",
        nextToken: ?string,
      |},
      owner: ?string,
    |},
    time: string,
    weekDay: string,
    stop: string,
    owner: ?string,
  |},
|};

export type OnUpdateTimeSubscriptionVariables = {|
  owner: string,
|};

export type OnUpdateTimeSubscription = {|
  onUpdateTime: ? {|
    __typename: "Time",
    id: string,
    transport: ? {|
      __typename: "Transport",
      id: string,
      number: string,
      direction: string,
      time: ? {|
        __typename: "ModelTimeConnection",
        nextToken: ?string,
      |},
      owner: ?string,
    |},
    time: string,
    weekDay: string,
    stop: string,
    owner: ?string,
  |},
|};

export type OnDeleteTimeSubscriptionVariables = {|
  owner: string,
|};

export type OnDeleteTimeSubscription = {|
  onDeleteTime: ? {|
    __typename: "Time",
    id: string,
    transport: ? {|
      __typename: "Transport",
      id: string,
      number: string,
      direction: string,
      time: ? {|
        __typename: "ModelTimeConnection",
        nextToken: ?string,
      |},
      owner: ?string,
    |},
    time: string,
    weekDay: string,
    stop: string,
    owner: ?string,
  |},
|};